/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;

/**
 *
 * @author j.pluzinska
 */
public class address {
    
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    
    public String getulica() 
    {
	return this.ulica;
    }

    public void setulica(String ulica) 
    {
	this.ulica = ulica;
    }
    public String getmiasto() 
    {
	return this.miasto;
    }

    public void setmiasto(String miasto) 
    {
	this.miasto = miasto;
    }
    
    public int getnumer_domu() 
    {
	return this.numer_domu;
    }
    
    public int getnumer_mieszkania() 
    {
	return this.numer_mieszkania;
    }
    
    public void setnumer_domu(int nr) 
    {
	this.numer_domu = nr;
    }
    
    public void setnumer_mieszkania(int nr) 
    {
	this.numer_mieszkania = nr;
    }
    public void wypiszadres()
    {
        System.out.println("Adres:");
        System.out.print(this.ulica);
        System.out.print(" ");
        System.out.print(this.numer_domu);
        if (this.numer_mieszkania!=0)
        {
            System.out.print("/");
            System.out.print(this.numer_mieszkania);
        }
        System.out.println(this.miasto);
    }
}
