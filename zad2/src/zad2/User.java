/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;

/**
 *
 * @author j.pluzinska
 */
public abstract class User {
    private String login;
    private String haslo;
	
    public String getlogin() 
    {
	return this.login;
    }

    public void setlogin(String login) 
    {
	this.login = login;
    }
    
    public String gethaslo() 
    {
	return this.haslo;
    }

    public void sethaslo(String haslo) 
    {
	this.haslo = haslo;
    }
}
