/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;

/**
 *
 * @author j.pluzinska
 */
public class Zad2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        person alek = new person();
        alek.setimie("Alek");
        alek.setnazwisko("Nowak");
        alek.setlogin("al");
        alek.sethaslo("al_the_best_of");
        address a = new address();
        a.setmiasto("Sopot");
        a.setnumer_domu(7);
        a.setulica("Nowa");
        alek.addaddress(a);
        permission p = new permission();
        p.setpermission("modyfikacja");
        permission p2 = new permission();
        p2.setpermission("podglad");
        role r = new role();
        r.setrole("admin");
        r.addpermission(p2);
        r.addpermission(p);
        alek.addrole(r);
        alek.addTel(123456789);
        alek.addTel(502391234);
        System.out.println(alek.getlogin());
        System.out.println(alek.gethaslo());
        System.out.println(alek.getimie());
        System.out.println(alek.getnazwisko());
        System.out.print(" ");
        alek.listofaddresses();
        alek.listoftels();
        alek.listofroles();
        
    }
    
}
