/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zad2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author j.pluzinska
 */
public class person extends User{
    private String imie;
    private String nazwisko;
    private List listA = new ArrayList();
    private List listTel = new ArrayList();
    private List listRole = new ArrayList();
    
    public String getimie() 
    {
	return this.imie;
    }

    public void setimie(String imie) 
    {
	this.imie = imie;
    }
    public String getnazwisko() 
    {
	return this.nazwisko;
    }

    public void setnazwisko(String nazwisko) 
    {
	this.nazwisko = nazwisko;
    }
    public void addaddress(address ad) 
    {
	this.listA.add(ad);
    }
    public void addrole(role r) 
    {
	this.listRole.add(r);
    }
     public void addTel(int tel) 
    {
	this.listTel.add(tel);
    }
    public void deleteTel(int tel) 
    {
	this.listTel.remove(tel);
    }
     public void deleteaddress(address ad) 
    {
	this.listA.remove(ad);
    }
     public void deleterole(role r) 
    {
	this.listRole.remove(r);
    }
    public void clearaddress() 
    {
	this.listA.clear();
    }
    public void clearTel() 
    {
	this.listTel.clear();
    }
    public void clearRole() 
    {
	this.listRole.clear();
    }
    public address getaddress(int num) 
    {
        Iterator iterator = this.listA.iterator();
        int i=0;
        address element = new address();
        element.setulica("brak");
        element.setnumer_domu(0);
        element.setnumer_mieszkania(0);
        element.setmiasto("brak");
        while (iterator.hasNext())
        {
            iterator.next();
            i++;
        }
        if (num<=i)
        
            element = (address) this.listA.get(num);
        
        else
            
            System.out.print("Lista jest pusta");
          
	return element;
    }
     public role getrole(int num) 
    {
        Iterator iterator = this.listRole.iterator();
        int i=0;
        role element = new role();
        element.setrole("brak");
        while (iterator.hasNext())
        {
            iterator.next();
            i++;
        }
        if (num<=i)
        
            element = (role) this.listRole.get(num);
        
        else
            
            System.out.print("Lista jest pusta");
          
	return element;
    }
    public int getTel(int num) 
    {
        Iterator iterator = this.listTel.iterator();
        int i=0;
        int tel=0;
        while (iterator.hasNext())
        {
            iterator.next();
            i++;
        }
        if (num<i)
        
            tel = (int) this.listTel.get(num);
        
        else
            
            System.out.print("Lista jest pusta");
          
	return tel;
    }
    
    public void listofaddresses() 
    {
        Iterator iterator = this.listA.iterator();
        address element = new address();
        int i=0;
        while (iterator.hasNext())
        {
            System.out.print("Adres");
            System.out.print(i);
            System.out.println(":");
            element = (address) this.listA.get(i);
            element.wypiszadres();
            iterator.next();
            i++;
        }
	
        
    }
    public void listofroles() 
    {
        Iterator iterator = this.listRole.iterator();
        role element = new role();
        int i=0;
        while (iterator.hasNext())
        {
            System.out.print("Rola:");
            System.out.print(i);
            System.out.println(":");
            element = (role) this.listRole.get(i);
            System.out.println(element.getrole());
            element.listofpermissions();
            iterator.next();
            i++;
        }
	
        
    }
    public void listoftels() 
    {
        Iterator iterator = this.listTel.iterator();
        int element = 0;
        int i=0;
        while (iterator.hasNext())
        {
            System.out.print("Tel:");
            System.out.print(i);
            System.out.println(":");
            element = (int) this.listTel.get(i);
             System.out.println(element);
             iterator.next();
            i++;
        }
	
        
    }
    
}
